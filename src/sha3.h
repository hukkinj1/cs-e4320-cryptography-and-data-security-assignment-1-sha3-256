/* Implement the following API. Do NOT modify the given prototypes. */

/* Compute the SHA-3 hash for a message.
 *
 * d - the output buffer (allocated by the caller)
 * s - size of the output buffer in bits
 * m - the input message
 * l - size of the input message in bits
 */
void sha3(unsigned char *d, unsigned int s, const unsigned char *m,
      unsigned int l);

/* You can add your own functions below this line.
 * Do NOT modify anything above. */

int get_column_array_index(int x, int z);
int get_state_array_index(int x, int y, int z);

void set_bit_zero(unsigned char *array, int n);
void set_bit_one(unsigned char *array, int n);
void set_bit(unsigned char *array, int n, int value);
int get_bit(const unsigned char *array,  int n);
void get_bits_from_index(unsigned char *output, const unsigned char *input, int first_bit_index, int amount_of_bits);
void write_bits_to_index(unsigned char *output, const unsigned char *input, int first_written_bit, int amount_of_bits);

void Rnd(unsigned char *output, const unsigned char *input, int round);
void keccak_p_1600_24(unsigned char *output, const unsigned char *input);
void keccak(const int c, unsigned char *output, int output_len, const unsigned char *input, int input_len);

void step_mapping_1_theta(unsigned char *output, const unsigned char *input);
void step_mapping_2_rho(unsigned char *output, const unsigned char *input);
void step_mapping_3_pi(unsigned char *output, const unsigned char *input);
void step_mapping_4_chi(unsigned char *output, const unsigned char *input);
void step_mapping_5_iota(unsigned char *output, const unsigned char *input, int round);

int mod (int a, int b);